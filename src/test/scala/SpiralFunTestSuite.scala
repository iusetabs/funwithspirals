import SpiralFun.{Point, manhattanDistance, pointGivenValueInSpiral}
import org.scalatest.FunSuite

/**
 * TestSuite which tests the following
 *  1. Given any value in an NxN matrix, we return the correct coordinates for said value
 *  2. Give any valie in an NxN matrix, we return the correct distance from the center
 *  */

class SpiralFunTestSuite extends FunSuite {

  test("The point for value 20 given a 5x5 spiral matrix should be (0,1)") {
    val coordinatesForTwenty = pointGivenValueInSpiral(19, Point(2,2))
    assert(coordinatesForTwenty.x == 0, "The X coordinate calculated was incorrect")
    assert(coordinatesForTwenty.y == 1, "The Y coordinate calculated was incorrect")
  }

  test("The point for value 15 should be given a 5x5 spiral matrix should be (2,4)") {
    val coordinatesForFifteen = pointGivenValueInSpiral(14, Point(2,2))
    assert(coordinatesForFifteen.x == 2, "The X coordinate calculated was incorrect")
    assert(coordinatesForFifteen.y == 4, "The Y coordinate calculated was incorrect")
  }

  test("The distance from the center for value 12 given a 5x5 spiral matrix should be 3") {
    val valueInTheMatrix = 12
    val centerPoint = Point(2,2)
    val coordinatesForValue = pointGivenValueInSpiral(valueInTheMatrix, centerPoint)
    assert(manhattanDistance(centerPoint, coordinatesForValue) == 3, "The wrong distance from the center was calculated")
  }

  test("The distance from the center for value 23 given a 5x5 spiral matrix should be 2") {
    val valueInTheMatrix = 23
    val centerPoint = Point(2,2)
    val coordinatesForValue = pointGivenValueInSpiral(valueInTheMatrix-1, centerPoint)
    assert(manhattanDistance(centerPoint, coordinatesForValue) == 2, "The wrong distance from the center was calculated")
  }

  test("The distance from the center for value 1024 given a 33x33 spiral matrix should be 31") {
    val valueInTheMatrix = 1024
    val centerPoint = Point(16,16)
    val coordinatesForValue = pointGivenValueInSpiral(valueInTheMatrix-1, centerPoint)
    assert(manhattanDistance(centerPoint, coordinatesForValue) == 31, "The wrong distance from the center was calculated")
  }

  test("The distance from the center for value 1 given a 33x33 spiral matrix should be 0") {
    val valueInTheMatrix = 1
    val centerPoint = Point(16,16)
    val coordinatesForValue = pointGivenValueInSpiral(valueInTheMatrix-1, centerPoint)
    assert(manhattanDistance(centerPoint, coordinatesForValue) == 0, "The wrong distance from the center was calculated")
  }

  test("The distance from the center for value 2 given a 33x33 spiral matrix should be 1") {
    val valueInTheMatrix = 2
    val centerPoint = Point(16,16)
    val coordinatesForValue = pointGivenValueInSpiral(valueInTheMatrix-1, centerPoint)
    assert(manhattanDistance(centerPoint, coordinatesForValue) == 1, "The wrong distance from the center was calculated")
  }

  test("The distance from the center for value 368078 given some 607x607 spiral matrix should be 371") {
    val valueInTheMatrix = 368078
    val centerPoint = Point(303,303)
    val coordinatesForValue = pointGivenValueInSpiral(valueInTheMatrix-1, centerPoint)
    assert(manhattanDistance(centerPoint, coordinatesForValue) == 371, "The wrong distance from the center was calculated")
  }

}
