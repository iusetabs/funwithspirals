import scala.annotation.tailrec

object SpiralFun extends App {

  /** Simply represents a point in the spiral matrix */
  case class Point(x: Int, y: Int) {
    override def toString: String = {
      s"x: $x, y: $y"
    }
  }

  private val widthAndHeightOfMatrix = Integer.parseInt(args.headOption.getOrElse(throw new Exception(s"Missing input number! Please provide N")))
  private val valueInMatrixToCalculateDistanceFrom = Integer.parseInt(args.lift(1).getOrElse(
    throw new Exception(s"Missing the value in the spiral to calculate the distance from!"))
  )

  // check that the supplied value can actually be in the spiral matrix
  assert(Math.sqrt(valueInMatrixToCalculateDistanceFrom) <= widthAndHeightOfMatrix,
    s"Value $valueInMatrixToCalculateDistanceFrom will not be in a ${widthAndHeightOfMatrix}X${widthAndHeightOfMatrix} spiral matrix. " +
      s"The N value is too small. Please supply a bigger N value.")

  private val centerCoordinates: Point = Point(widthAndHeightOfMatrix/2, widthAndHeightOfMatrix/2)

  /** returns the Manhattan distance between two points */
  def manhattanDistance(pointA: Point, pointB: Point): Int = {
    Math.abs(pointB.x - pointA.x) + Math.abs(pointB.y - pointA.y)
  }

  /** returns the Point for the given value in the spiral
   * @param valueInSpiral: The value in the spiral
   * @param centerPoint: This coordinates of the center point of the spiral */
  def pointGivenValueInSpiral(valueInSpiral: Int, centerPoint: Point = centerCoordinates): Point = {
    pointGivenNumberOfOperations(valueInSpiral-1, 1, centerPoint)
  }

  /** Take the below 3X3 matrix. Each value has an x, y coordinate, 7 is at point ( 0, 0 ) and the center point is ( 1, 1 )
   *  5   4   3
   *  6   1   2
   *  7   8   9
   *
   *  We can represent the points of each value in the spiral matrix as belonging to certain "arms"
   *  -   -   -   4
   *  3   3   2   4
   *  3   1   2   4
   *  3   4   4   4
   *
   *  We can figure out the last coordinate for each arm in the matrix when we know the center coordinates
   *  Note: Point a is the x coordinate of the center point
   *  Note: Point b is the y coordinate of the center point
   *  Arm 1 - last value of arm 1 lies at ( a + 1, b + 1).
   *  Arm 2 - last value of arm 2 lies at ( a + 1 - 2, b + 1 - 2 ).
   *  Arm 4 - last value of arm 4 lies at ( a + 1 - 2 + 3 - 4, b + 1 - 2 + 3 - 4).
   *
   *  Notice two things:
   *   First: with each arm, we add or subtract a variation which is just incremental increase from the previous number.
   *   Second: with each arm the sign of the variation should change, arm 1 is +, arm 2 is -, arm 3 is + and so forth.
   *  Using this knowledge, we can determine the coordinates of values in the matrix
   *
   *  @param numOfOperations:
   *  As the values in the spiral increase linearly, we know the number of times the x or y coordinate changes from the center is the input value from the user - 1.
   *  This value is recursively looped on and we subtract the amount of points on each arm from the number of steps left to get to the desired coordinate.
   *  @param variation: beings at 1 and is the same as the arm number
   *  @param pointToReturn: this is the final point which is passed back to the function recursively
   *  */
  @tailrec
  private def pointGivenNumberOfOperations(numOfOperations: Int, variation: Int, pointToReturn: Point): Point = {
    // counts how many points we move along on each arm arm
    var counter = 0

    def howManyMovesToEndOfArmOrDesiredPoint() : Int = {
      if ( counter + Math.abs(variation) <= numOfOperations ) {
        counter+=Math.abs(variation)
        variation
      } else { // the desired point may not be the last point on each arm
        val maxVariation = numOfOperations - counter
        counter+=Math.abs(maxVariation)
        if ( variation < 0 )
          - maxVariation
        else {
          maxVariation
        }
      }
    }

    if (numOfOperations <= 0 ) {
      pointToReturn
    } else {
      // will be the end of the current arm or the desired point which is along the arm
      val newXCord = pointToReturn.x + howManyMovesToEndOfArmOrDesiredPoint()
      val newYCord = pointToReturn.y + howManyMovesToEndOfArmOrDesiredPoint()

      // switches the sign for the next arm
      val newVariation = if ( variation < 0 ) {
        Math.abs(variation)+1
      }  else {
        -(variation+1)
      }
      pointGivenNumberOfOperations(numOfOperations-counter, newVariation, pointToReturn.copy(x = newXCord, y = newYCord))
    }
  }

  def distanceFromCenterGivenValue(valueInSpiral: Int, centerPoint: Point): Int = {
    val pointOfNumber = pointGivenValueInSpiral(valueInSpiral)
    manhattanDistance(centerPoint, pointOfNumber)
  }

  val distanceFromCenter = distanceFromCenterGivenValue(valueInMatrixToCalculateDistanceFrom, centerCoordinates)
  println(s"From location $widthAndHeightOfMatrix the distance is $distanceFromCenter.")
}
