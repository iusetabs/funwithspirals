## James Nolan ( O'Nualláin ) Spiral Fun! 
### Scala Proficiency Test
* Simple scala script which returns the distance from a value in the spiral matrix to the center

#### Build and execution instructions
* To build, simply execute ```scalac src/main/scala/SpiralFun.scala```
* To run, simply execute ```scala SpiralFun``` [N] [Value]

### Quick implementation details 
* I spent most time working this problem out on paper
* At first, I was thinking of user maps to store values at specific coordinates
* However, for large values this would not be a optimal solution, so I came at it from an algorithmic approach
* There are only two files of interest. SpiralFun and SpiralFunTestSuite. 
* I wrote the functions in a way that they were easy to test and that various components could be re-used for other purposes.
